import 'dart:async';

import 'package:flutter/material.dart';

void main() {
  runApp(const AAMainApp());
}

class AAMainApp extends StatelessWidget {
  const AAMainApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // var size = MediaQuery.of(context).size;
    // 274 251
    // contex.set
    return MaterialApp(
      title: 'Errors repporter',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const AHomeUI(title: 'Errors reporter'),
    );
  }
}

class AHomeUI extends StatefulWidget {
  final String title;
  const AHomeUI({super.key, required this.title});
  @override
  State<AHomeUI> createState() => _AHomeUIState();
}

class _AHomeUIState extends State<AHomeUI> {
  final Stopwatch _stopwatch = Stopwatch();
  String _seconds = '0';
  String _millis = '0';

  late Timer _timer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            timeWidget(context),
            buttons(context),
            const _ButtonWithCount(text: "success", color: Colors.green),
            const _ButtonWithCount(text: "failures", color: Colors.red),
          ],
        ),
      ),
    );
  }

  Widget buttons(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          startButton(context),
          stopButton(context),
        ],
      ),
    );
  }

  Widget startButton(BuildContext context) {
    return FloatingActionButton(
      onPressed: _startTimer,
      tooltip: 'Start',
      child: const Icon(Icons.start_rounded),
    );
  }

  Widget stopButton(BuildContext context) {
    return FloatingActionButton(
      onPressed: _stopTimer,
      tooltip: 'Stop',
      child: const Icon(Icons.stop_circle_rounded),
    );
  }

  Widget timeWidget(BuildContext context) {
    return Text(
      '$_seconds:$_millis',
      style: const TextStyle(color: Colors.black, fontSize: 40),
    );
  }

  void _startTimer() {
    _stopwatch.reset();
    _stopwatch.start();
    _timer = Timer.periodic(const Duration(milliseconds: 50), (Timer t) {
      setState(() {
        num ms = (_stopwatch.elapsedMilliseconds % 1000);
        _millis = ms.toString();
        if (_millis.length == 1) {
          _millis = "0$_millis";
        } else {
          _millis = _millis.substring(0, 2);
        }
        _seconds = _stopwatch.elapsed.inSeconds.toString();
      });
    });
  }

  void _stopTimer() {
    _timer.cancel();
    _stopwatch.stop();
  }
}

class _ButtonWithCount extends StatefulWidget {
  final String text;
  final Color color;
  const _ButtonWithCount({required this.text, required this.color});

  @override
  State<_ButtonWithCount> createState() => _ButtonWithCountState();
}

class _ButtonWithCountState extends State<_ButtonWithCount> {
  int _counter = 0;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        counterText(context),
        const VerticalDivider(),
        incrementButton(context),
        const VerticalDivider(),
        decrementButton(context)
      ],
    );
  }

  Widget counterText(BuildContext context) {
    String ctrText = _counter.toString();
    if (ctrText.length == 1) {
      ctrText = "  $ctrText";
    } else if (ctrText.length == 2) {
      ctrText = " $ctrText";
    }
    return RichText(
      text: TextSpan(
        text: "${widget.text}:  ",
        style: const TextStyle(color: Colors.black, fontSize: 32),
        children: <TextSpan>[
          TextSpan(text: ctrText, style: TextStyle(color: widget.color)),
        ],
      ),
    );
  }

  Widget incrementButton(BuildContext context) {
    return SizedBox(
        width: 30,
        height: 30,
        child: FloatingActionButton(
          onPressed: _increment,
          tooltip: 'Add',
          child: const Icon(Icons.add_rounded),
        ));
  }

  Widget decrementButton(BuildContext context) {
    return SizedBox(
        width: 30,
        height: 30,
        child: FloatingActionButton(
          onPressed: _decrement,
          tooltip: 'Remove',
          child: const Icon(Icons.remove_rounded),
        ));
  }

  void _increment() {
    setState(() {
      _counter++;
    });
  }

  void _decrement() {
    setState(() {
      _counter--;
    });
  }
}
